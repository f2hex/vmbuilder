# VM Builder

A quite simpler provisiong tool to create on the fly a Linux VM on Azure to be used for building processes.

## Configuration

Just create a configuration file named `.vmbuilder` under your HOME dir and put the right values there like the example below:

```bash
# VM builder configuration
RES_PREFIX="xx"
REGION="westeurope"
OS_IMAGE="Canonical:0001-com-ubuntu-server-hirsute:21_04:21.04.202201180"
VM_FLAVOR="Standard_A4_v2"
ROOT_DISK_SIZE=50
```

* `RES_PREFIX` is just a prefix that should guarantee a unique name for the VM DNS public name, the script will add at the end the
string "`-builder`".
* `REGION` is the preferred Azure region where to run the VM
* `OS_IMAGE` is the Linux OS image to be used for the build work
* `VM_FLAVOR` is the Azure VM instance type to be used for the builder
* `ROOT_DISK_SIZE` is the size of the ephemeral root disk of the VM

## How to use it

After the configuration file is created you can run the script by specifying the `ssh` to be used to log into the VM like in this way:

```bash
./vmbuilder start -s ~/.ssh/id_rsa-f2builder.pub
```

Assuming there are no errors after about a minute or more the VM should be up and running and the script will print out the public DNS name of the running VM.

To delete the VM just run the following command (be sure that you have not changed the `~/.vmbuilder` file...) after the last start command:

```bash
./vmbuilder stop
```

## Using the VM

To login into the VM just perform the ssh command using the ssh key provided when you start it:

```
ssh -i ~/.ssh/id_rsa-f2builder.pub xx-builder.westeurope.cloudapp.azure.com
```

## Stop the VM (terminate it)

Quite simple:

```
./vmbuilder stop
```

The VM is terminated and all the associated resources on the Azure cloud are reclaimed.



